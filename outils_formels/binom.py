from numpy import math, prod, power, exp

def fact(n):
	"By convention the result of factor 0 would be equal to 1"
	assert n>=0
	return math.factorial(n)

def ABin(p,n):
	"number of p among n arrangements"
	return fact(n)/fact(n-p)

def CBin(p,n):
	"number of ways to choose a p from n"
	assert 0 <= p and p<=n 
	return fact(n)/(fact(n-p)*fact(p))

def multCBin(n,p_arr):
	"multinomial coefficient"
	assert sum(p_arr)==n
	return fact(n)/prod([fact(e) for e in p_arr])

# n : Number of independent trials
# p : Probability of success on each trial
# k : P(X=k)
# return : binomial distribution probability
def Binomial(n,p,k):
	assert 0 <= k and k <= n 
	assert 0 <= p and p <= 1
	return CBin(k,n)*power(p,k)*power(1 - p,n-k)

# ln : lambda
# k : P(X=k)
# return : Poisson distribution probability
def Poisson(lb, k):
	assert 0 <= k and k<=lb and 0 < lb 
	return (power(lb,k) * exp(-lb))/fact(k)

# r :
# p : 
# k : 
def Pascal(r,p,k):
	assert 0 <= r and r <= k
	return CBin(r-1,k-1)*power(p,r)*power(1-p,k-r)

def geometrique(p,k):
	assert 0 <= p and p <= 1
	assert k != 0
	return power(1-p,k-1)*p