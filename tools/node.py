class Node :
    def __init__(self, 	data) :
        self.__depth = 0
        self.__data = data
        self.__children = []

    def get_data(self):
        return self.__data

    def set_data(self, data):
        self.__data = data
        return self

    def hisChild(self, data):
        for node in self.__children:
            if node.get_data() == data :
                return True
        return False

    def add(self, data, with_duplicate=False):
        if not with_duplicate and self.hisChild(data) :
            return self.find(data)
        n = Node(data)
        n.__depth = self.__depth + 1
        self.__children.append(n)
        return n

    def addNode(self, node, with_duplicate=False):
        if not with_duplicate and self.hisChild(node.get_data()) :
            return node
        self.__children.append(node)
        return node

    def getChild(self, pos = 0):
        return self.__children[pos]

    def size(self):
        res = 1
        for e in self.__children :
            res += e.size()
        return res

    def find(self, data):
        if self.__data == data :
            return self
        else:
            res = None
            for e in self.__children:
                res = e.find(data)
                if res != None :
                    return res
            return res

    def __print(self,d, stack=[]):
        if self in stack :
            print("\t"*d,str(self.__data)+"*")
            return
        stack.append(self)
        print('\t'*self.__depth,self.__data)
        for item in self.__children:
            item.__print(self.__depth+1,stack)

    def print(self):
        self.__print(self.__depth)
        print()

class Tree :
    def __init__(self):
        self.__root = Node("root")

    def size(self):
        return self.__root.size()

    def add(self, data):
        return self.__root.add(data)

    def getChild(self, pos = 0):
        return self.__root.getChild(pos)

    def find(self, data):
        return self.__root.find(data)

    def print(self):
        self.__root.print()

class Graph :
    def __init__(self):
        self.__start = Node("start")

    def route(self,a,b):
        if a == b :
            node = self.__start.find(a)
            if node == None : node = self.__start.add(a)
            node.addNode(node)
        else :
            nA = self.__start.find(a)
            nB = self.__start.find(b)
            if nA == None : nA = self.__start.add(a)
            if nB == None : nA.add(b)
            else : nA.addNode(nB)

    def print(self):
        self.__start.print()
