import random as rd
from math import log

def sum_of_raising_hands(n, p):
    '''
    @arg n : (int) number of element
    @arg p : (int) probability
    @return n : (int) P(N=e) with P(e) = p/n
    '''
    return sum([1 if rd.randint(1,n+1) <= p else 0 for i in range(n)])

def las_vegas_election(n):
    time = 0
    while True :
        time+=1
        if sum_of_raising_hands(n,1) == 1 :
            break
    return (time, True)

def monte_carlo_election(n):
    counter = 0
    a_elected = False
    while counter < log(n,2) :
        if sum_of_raising_hands == 1:
            a_elected = True
            break
        counter+=1
    return (counter, a_elected)