def printer(G,node = 0, stack=[], depth = 0):
    print("\t"*depth if depth>0 else "",node, "->",G[node][1])
    if node not in stack:
        stack.append(node)
        for n in G[node][1] :
            printer(G,n,stack,depth+1)

def main():
    G = [(0,[2]), (1,[0,2,4]),(2,[3]),(3,[0,1]),(4,[5,6,7]),(5,[1]),(6,[1]),(7,[1])]
    printer(G,1)


if __name__ == '__main__':
    main()